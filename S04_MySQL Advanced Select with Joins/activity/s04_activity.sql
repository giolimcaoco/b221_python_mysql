--a
SELECT * FROM songs WHERE song_name LIKE "%d%";

--b
SELECT * FROM songs WHERE length < 230;

--c
SELECT album_title, song_name, length  FROM albums
	JOIN songs ON albums.id = songs.album_id;

--d
SELECT * FROM albums
    JOIN artists ON albums.artist_id = artists.id
    WHERE albums.album_title LIKE '%a%';

--e
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

--f
SELECT * FROM albums
    JOIN songs ON songs.album_id = albums.id
    ORDER BY albums.album_title DESC;