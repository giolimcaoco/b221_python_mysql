-- add new records

-- add artists

INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");


-- add albums

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Fearless (Taylor's Version)",
	"2021-04-09",
	3
);


INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Red (Taylor's Version)",
	"2021-11-12",
	3
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"A Star is Born",
	"2018-01-01",
	4
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Born This Way",
	"2011-01-01",
	4
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Purpose",
	"2015-01-01",
	5
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Dangerous Woman",
	"2016-01-01",
	6
);


-- add songs

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Fearless (Taylor's Version)",
	246,
	"Pop Rock",
	3
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"All Too Well (Taylor's Version)",
	253,
	"Love Song",
	4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Black Eyes",
	151,
	"Rock",
	5
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Born This Way",
	252,
	"Pop",
	6
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Sorry",
	212,
	"Pop Rock",
	7
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Into You",
	242,
	"EDM",
	8
);


-- ADVANCED SELECTED

-- exclude records (not operator)

SELECT * FROM songs WHERE id != 5;

-- GT / GTE
SELECT * FROM songs WHERE id >=7;

-- LT / LTE
SELECT * FROM songs WHERE id <=7;

-- get the specific id (or)

SELECT * FROM songs WHERE id = 5 OR id = 7 OR id = 10;

-- get the id (IN) // simplified instead of OR

SELECT * FROM songs WHERE id IN (5,7,10);


-- find partial matches

SELECT * FROM songs WHERE song_name LIKE "%e"; -- select a keyword/pattern from the end of the string

SELECT * FROM songs WHERE song_name LIKE "b%"; -- select a keyword/pattern from the start of the string

SELECT * FROM songs WHERE song_name LIKE "%a%"; -- select a keyword/pattern from the any part of the string

-- Sort records

SELECT * FROM songs ORDER BY song_name ASC; -- ASC - Ascending

SELECT * FROM songs ORDER BY song_name DESC; -- DESC - Descending

-- get distinct records (show all unique values)

SELECT DISTINCT genre from songs;

-- count()

SELECT COUNT(*) FROM songs WHERE genre = "Pop Rock";



-- [TABLE JOINS] --

-- combine artists and albums tables (JOIN/INNER JOIN)

SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id;

-- left join
SELECT * FROM artists
	LEFT JOIN albums ON artists.id = albums.artist_id;

-- right join
SELECT * FROM artists
	RIGHT JOIN albums ON artists.id = albums.artist_id;

-- join multiple tables

SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;