-- CRUD Operations

-- Insert/add/creating a record
INSERT INTO artists (name) VALUES ("Rivermaya");

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-01-01", 1);

INSERT INTO songs (song_name, length, genre, album_id)
	VALUES ("Kundiman", 234, "OPM", 1);

INSERT INTO songs (song_name, length, genre, album_id)
	VALUES ("Kisapmata", 259, "OPM", 1);

-- mini activity

INSERT INTO artists (name) VALUES ("Bamboo");
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Light Peace Love", "2005-01-01", 2);

INSERT INTO songs (song_name, length, genre, album_id)
	VALUES ("Hallelujah", 450, "OPM", 2);


-- Selecting/Retrieving Records

-- display song_name of all songs
SELECT song_name FROM songs;

-- display all columns of songs table
SELECT * FROM songs;

-- display song_name and genre of all songs
SELECT song_name, genre FROM songs;

-- display title of OPM

SELECT song_name FROM songs WHERE genre = "OPM";

-- WHERE can also be paired with AND or OR

SELECT song_name, length FROM songs WHERE length > 240 AND genre = "OPM";

SELECT song_name, length FROM songs WHERE length > 240 OR genre = "OPM";

-- updating records

UPDATE songs SET length = 240 WHERE song_name = "Kundiman";
-- removing WHERE clause will result into ALL columns being updated

-- deleting records

DELETE FROM songs WHERE genre = "OPM" and song_name = "Kisapmata";

-- removing of WHERE clause will result to the deletion of ALL rows