-- MySQL - Structured Query Language

/*

Steps to run mySQL

1. Run MySQL and Apache in XAMPP
2. Run specific databases - MariaDB
	- Launch Termnial/Command Prompt and run the following command

		mysql -u root - PC
		mysql -u root -p - MacOS

-- ; (semi colon) every command line requires semi colon 

-- set PATH=%PATH%;C:\XAMPP\mysql\bin


3. Commands to use in DBMS

		***commands are not case sensitive in mySQL

*/

SHOW DATABASES; -- to show DBs
CREATE DATABASE music_db; -- to create DBs
DROP DATABASE music_db; -- remove DB
USE music_db; -- select DB

-- create tables
-- table columns have the following format: 

-- [column name] [data_type] [other options]
	-- data_type
	-- INT - specifies that the column should accept integer data type
	-- VARCHAR(character limit) - variable characters; string; 
	-- DATE refers to YYYY-MM-DD
	-- TIME refers to HH:mm:ss
	-- DATETIME refers to YYYY:MM:DD HH:mm:ss
	-- NOT NULL means it is required
	-- AUTO_INCREMENT setting the id to have different value (1 -> 2 -> 3 ...)
	-- PRIMARY KEY (column_name) - typically IDs - unique identifiers for each rows in the table



CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number INT NOT NULL,
	email VARCHAR(50),
	address VARCHAR(50),
	PRIMARY KEY(id)
);

/*
Mini activity

create a table for artist
*/

CREATE TABLE artists(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL, 
	PRIMARY KEY(id)
);

-- CREATE TABLE albums(
-- 	id INT NOT NULL AUTO_INCREMENT,
-- 	album_title VARCHAR(50) NOT NULL,
-- 	date_released DATE NOT NULL,
-- 	artist_id INT NOT NULL,
-- 	PRIMARY KEY (id),
-- 	CONSTRAINT fk_album_artist_id
-- 	FOREIGH KEY (artist_id) REFERENCES artist(id)
-- 	ON UPDATE CASCADE
-- 	ON DELETE RESTRICT
-- );

	-- CONSTRAINT - optional command rule, in this context we used it to identify the FOREIGN KEY. Written in one line
	-- FOREIGN KEY - the connection between tables
	-- REFERENCES - refers to where the FK gets it value
	-- ON UPDATE CASCADE - every time the parent table updates, it should also affect the related table
	-- ON DELETE RESTRICT --  the tables cannot be immediately deleted when there are more tables related to it

CREATE TABLE albums (
	id INT NOT NULL AUTO_INCREMENT,
	album_title VARCHAR(50) NOT	NULL,
	date_released DATE NOT NULL,
	artist_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id
		FOREIGN KEY (artist_id) REFERENCES artists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE songs (
	id INT NOT NULL AUTO_INCREMENT,
	song_name VARCHAR(50) NOT NULL,
	length TIME NOT NULL,
	genre VARCHAR(50) NOT NULL,
	album_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_songs_album_id
		FOREIGN KEY (album_id) REFERENCES albums(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE playlists (
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	date_time_created DATETIME NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_playlist_user_id
		FOREIGN KEY	(user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE playlist_songs (
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_songs_playlist_id
		FOREIGN KEY (playlist_id) REFERENCES playlists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_playlists_songs_song_id
		FOREIGN KEY (song_id) REFERENCES songs(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);







